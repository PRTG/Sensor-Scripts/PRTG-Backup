Installation Instructions
=========================

To install the PRTG Backup sensor, unzip the installer package into the PRTG Executable directory, 
as outlined in Install_PRTG.md

To install and configure the sensor, pick the device (Usually the Probe device) and add
an "EXE/Script Advanced" and select "PRTGBackup.ps1" as the "EXE/Script".
The minimum parameters needed for the script is the destination for the backups.
"-BackupTargetPath "DEST" is any local fully qualified path(C:\Temp\Junk) or UNC path (\\Server\Share\Dir....)


The other active parameters are (Booleans are 0:False, 1:True):
	-BackupData 		= {0|1} Backup Sensor data True/False
	-BackupInstall 		= {0|1} Backup Install Files True/False
	-BackupCustomSensors= {0|1} Backup Custom Sensors, Lookups, Templates True/False
	-BackupCustom 		= {0|1} Backup Customizations True/False
	-CompressDataDirs 	= {0|1} Compress Sensor Data Directories

It is also recommended that you run the sensor once per day scheduled after midnight Local time(Relative to the Core Server setting)…
Create a custom Interval like: “@ UTC 01:00”
For more details, please see https://kb.paessler.com/en/topic/3723-can-i-set-a-sensor-to-run-at-a-specific-time

