PRTG Custom Sensor for backing up PRTG.
===========================================

This project contains all the files necessary to integrate the Custom Script as a [PRTG EXE/Script Advanced Sensor](https://www.paessler.com/manuals/prtg/exe_script_advanced_sensor).

The script will be installed as a Sensor with a parameter of the target directory.

Within the target directory, there will be a hierarchy as outlined below.

BackupLog   : Text output from backup process, one file per day... (RoboCopy output)
 * Data		: Files from PRTG DataDirectory
 * EXE		: Files from the PRTG Executable directory

./BackupLog:

yyyyMMdd_PRTGBackup.Log   : Backup output log from given day

./Data:
 * Configuration Auto-Backups
 * Monitoring Database
 * Ticket Database
 * Trap Database
 * ...

./EXE:
 * Custom Sensors
 * Download
 * PRTG Installer Archive
 * devicetemplates
 * lookups
 * snmplibs
 * webroot
 * ...


./EXE/webroot/includes:
* htmlfooter_custom_v2.htm
* htmlheader_custom_v2.htm

![Image of PRTG Backup Sensor Channelss](./Images/PRTGBackup_SensorChannels.png)


Download Instructions
=========================
 [A zip file containing all the files in the installation package can be downloaded directly using this link](https://gitlab.com/PRTG/Sensor-Scripts/PRTG-Backup/-/jobs/artifacts/master/download?job=PRTGDistZip)
.

Installation Instructions
=========================
[Generic PRTG custom sensor instructions INSTALL_PRTG.md](./INSTALL_PRTG.md)

To install the PRTG Backup sensor, unzip the installer package into the PRTG Executable directory,
as outlined in Install_PRTG.md

To install and configure the sensor, pick the device (Usually the Probe device) and add
an "EXE/Script Advanced" and select "PRTGBackup.ps1" as the "EXE/Script".

Custom interval
-------
It is recommended that you run the sensor once per day scheduled after midnight Local time(Relative to the Core Server setting)…

Create a custom Interval like: “@ UTC 01:00”
 On the Menu Setup -> System Administration -> Monitoring

 (Or http://YOUR-PRTG-SERVER-IP-or-FQDN/systemsetup.htm?tabid=1)

If you are on US Central Time (UTC -6) and want to set the Backup to go at 12:30am/00:30/CST which is 6:30/UTC : the interval is: "@ UTC 06:30"
Go to the Setting
![Image of PRTG Custom Interval Settings](./Images/CustomInterval.png)
[For more details, please see KB article "Can I set a sensor to run at a specific time?"](https://kb.paessler.com/en/topic/3723-can-i-set-a-sensor-to-run-at-a-specific-time)


The sensor must be added to a device that is executed on a PRTG Server, the natural choice would be the Probe Device on Local Probe:
![Image of where to add PRTG Backup Sensor](./Images/PRTGBackup_CreateSensor_Device.png)

Add "EXE/Script Advanced Sensor"
![Image of where to add PRTGCustom EXE Backup Sensor](./Images/PRTGBackup_CreateSensor.png)

Specific settings :
![Image of PRTG Backup Sensor Settings](./Images/PRTGBackup_SensorSettings.png)
The minimum parameters needed for the script is the destination for the backups.

<pre>
Minimum parameter required:
	-BackupTargetPath 	= Target directory for Executables and Custom script Files

Like: -BackupTargetPath "X:\PRTGBackup\CoreServerX"
The files will be copied into the target directory "X:\PRTGBackup\CoreServerX",
Executables and Data files will be put into separate subdirectories ("EXE" and "Data" respectively)

 OR
	-BackupExePath 		= Target directory for Executables and Custom script Files
	-BackupDataPath 	= Target directory for Configuration and Data Files

Like: -BackupDataPath "X:\ProgramData\PRTGBackup" -BackupExePath "X:\Program Files(x86)\PRTGBackup"
Where the Data And EXE target directories are fully qualified...
(The target could be a Cold-Standby PRTG server)

The sensor creates a backup log containing all the files backed up.
It will be located in a sub-directory "PRTGBackupLog" in the BackupDataPath.

The other active parameters are (Booleans are 0:False, 1:True):
	-BackupData 		= {0|1} Backup Sensor data True/False
	-BackupInstall 		= {0|1} Backup Install Files True/False
	-BackupCustomSensors    = {0|1} Backup Custom Sensors, Lookups, Templates True/False
	-BackupCustom 		= {0|1} Backup Customizations True/False
	-CompressDataDirs 	= {0|1} Compress Sensor Data Directories

If the target is another server (Cold Stand-By or other) you want to turn off
compression (-CompressDataDirs False).
The parameters would look something like:
 -BackupDataPath "\\PRTG-Cold\ProgramData" -BackupExePath "\\PRTG-Cold\PRTGB-ProgramDir" -CompressDataDirs 0


All paths can be a fully qualified local path(C:\Temp\Junk) or UNC path (\\\\Server\Share\Dir....)
 Requirement:
  All target paths must exist (IE must be created before the script is run, the script does not create the targe directories. )
</pre>

** Please Note **
 The most recent copy of the configuration("PRTG_Configuration.dat") file will be in the most recent PRTG Configuration Backup zip file(BackupDataPath\Configuration Auto-Backups\PRTG Configuration (Backup YYY-MM-DD).zip).
 You would have to extract the latest config file from the Zip in order to restore configuration.

 The problem with copying the config file directly is that the config file may be
inconsistent (as in PRTG is currently updating it)...
It would require :
 1) Passing PRTG API token to the script
 2) Call PRTG API to create a configuration Backup ("/api/savenow.htm").
 3) copy Config to local temp (to minimize time taken)
 4) Copy to target
I would recommend creating a scrip for starting the Cold backup...

 Process:
 1) Extract Config from latest PRTG Configuration backup
 2) Start PRTG Core servics ("net start PRTGCoreService")
 3) Start PRTG Probe servics ("net start PRTGProbeService")


Testing
===============
 Running the script from a Commandline (as administrator, since the Source paths are protected):
```powershell
powershell ./PRTGBackup.ps1 -BackupDataPath "c:\temp\Junk\DataPath" -BackupExePath "c:\temp\Junk\EXEPath"
```
Output:
```xml
<prtg>
	<!-- Summary: PRTG Installer Archive -->
	<result>
		<channel>PRTG Install files Files</channel>
		<value>4</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: Download -->
	<result>
		<channel>PRTG Downloaded files Files</channel>
		<value>2</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: Custom Sensors -->
	<result>
		<channel>Custom Sensors Files</channel>
		<value>54</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: lookups\custom -->
	<result>
		<channel>Custom Lookups Files</channel>
		<value>222</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: snmplibs -->
	<result>
		<channel>snmplibs Files</channel>
		<value>51</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: devicetemplates -->
	<result>
		<channel>devicetemplates Files</channel>
		<value>106</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: webroot\includes -->
	<result>
		<channel>Custom Header-Footer Files</channel>
		<value>8</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary:  -->
	<result>
		<channel>Custom Flow Rules Files</channel>
		<value>2</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: Configuration Auto-Backups -->
	<result>
		<channel>Config Auto-Backups Files</channel>
...
	</result>
	<result>
		<channel>Monitoring DB Directories</channel>
...
	</result>
	<result>
		<channel>Monitoring DB Skipped</channel>
...
	</result>
	<result>
		<channel>Monitoring DB Failed</channel>
		<value>0</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
		<LimitMaxWarning>1</LimitMaxWarning>
		<LimitMode>1</LimitMode>
	</result>
	<!-- Summary: Monitoring Database -->
	<result>
		<channel>Monitoring DB Files</channel>
		<value>0</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<!-- Summary: Trap Database -->
	<result>
		<channel>Trap DB Files</channel>
...
	</result>
	<!-- Summary: Syslog Database -->
	<result>
		<channel>Syslog DB Files</channel>
...
	</result>
	<!-- Summary: Toplist Database -->
	<result>
		<channel>Toplist DB Files</channel>
		<value>0</value>
...
	</result>
	<!-- Summary: System Information Database -->
	<result>
		<channel>SysInfo DB Files</channel>
		<value>0</value>
...
	</result>
	<!-- Summary: Ticket Database -->
	<result>
		<channel>Ticket DB Files</channel>
		<value>2</value>
...
	</result>
	<!-- Summary: Report PDFs -->
	<result>
		<channel>Report PDFs Files</channel>
		<value>0</value>
...
		<CustomUnit>#</CustomUnit>
	</result>
  <text>Backup script succeeded</text>
</prtg>
```


Troubleshooting
================
The Backup script is written in Powershell
In case there are issues with the script please follow the steps outlined in the [PRTG Powershell trouble shooting guideline](
https://kb.paessler.com/en/topic/71356-guide-for-powershell-based-custom-sensors)

The quickest fix for the "ExecutionPolicy" errors is to copy and paste the script content into a new file with the same name/extention...
(Since Windows detects the file as a "foreign" file and deem it unsafe.)
