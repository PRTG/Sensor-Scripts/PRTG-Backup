﻿<#
.SYNOPSIS
    Backup PRTG server Data and configurations

.DESCRIPTION
		This script will backup the your instance of PRTG's Data, Custom scripts, Sensors, 
		and Customizations to a different file location. 


.NOTES
    The customer or user is authorized to copy the script from the repository and use as they please. 
		The script is supplied AS-IS, no warranty expressed or implied. In particular, JR Andreassen, Paessler AG 
		assumes no liability for the function, the use and the consequences of the use of this freely available script.
    PowerShell is a product of Microsoft Corporation. PRTG is a product of Paessler AG.

.COMPONENT
    Requires Powershell v2.0+
    Requires Module(s):  System.web,  Microsoft.Win32.RegistryKey

.LINK
    https://gitlab.com/PRTG/Sensor-Scripts/PRTG-Backup

.Parameter PRTGSourceServer
    Source PRTG Server name (Default: Localhost, local host only supported at this time)

.Parameter BackupTargetPath
		Backup Target path, format "Drive:\path" or UNC: "\\Server\Share"...
		When specified, DataPath will be set to "$BackupTargetPath\Data" and DataPath will be set to "$BackupTargetPath\Exe"

.Parameter BackupDataPath
    Backup Target path for PRTG files from Data Directory, format "Drive:\path" or UNC: "\\Server\Share"

.Parameter BackupEXEPath
    Backup Target path for PRTG files from Program Directory, format "Drive:\path" or UNC: "\\Server\Share"

.Parameter PRTGTargetServer
    Specifies the name of the snapshot you want to retrieve

.Parameter DomainUser
		Credentials: Domain user for access to target machine (Not yet in use) (Default: CurrentUser)

.Parameter DomainPassword
		Credentials: Password for DomainUser(Not yet in use)

.Parameter BackupData
		Flag to indicate if you want to backup Data Directory (Default: True)

.Parameter BackupInstall
		Flag to indicate if you want to backup Installation Files (Default: True)
		
.Parameter BackupCustomSensors
		Flag to indicate if you want to backup Custom Sensors Directory (Default: True)
		
.Parameter BackupCustom
		Flag to indicate if you want to backup Customization Files (Default: True)
		
.Parameter CompressDataDirs
		Flag to indicate if you want to compress Data Directory (Default: True)
		
.Parameter debuglevel
		Flag to indicate Debug level (0-n) (Default: 0)
		
.Outputs		
		PRTG Custom sensor XML describing what was backed up

#>
###############################################################################
#PRTGBackup.ps1
# prtg Backup Script
#
#
###############################################################################
#
# For trouble shooting please see:
# https://kb.paessler.com/en/topic/20023-which-powershell-version-is-used-by-prtg-when-executing-exe-script-custom-sensors
###############################################################################
# script parameters
Param (
	[string]$PRTGSourceServer = "localhost",
	[string]$BackupTargetPath = $null,
	[string]$BackupDataPath = $null,
	[string]$BackupEXEPath = $null,
	[string]$PRTGTargetServer = $null,
	[string]$DomainUser       = $env:USERDOMAIN+"\"+$env:USERNAME,
	[Securestring]$DomainPassword   = $null,
	[Boolean]$BackupData = $true,
	[Boolean]$BackupInstall = $true,
	[Boolean]$BackupCustomSensors = $true,
	[Boolean]$BackupCustom = $true,
	[Boolean]$CompressDataDirs = $true,
	[string]$PRTGSrcDataPath = $null,
	[string]$PRTGSrcEXEPath = $null,
    [int]$debuglevel = 0
)

########################################################################################
# PreCondition checks
########################################################################################
function Get-PSVersion {
    if (test-path variable:psversiontable) {$psversiontable.psversion} else {$host.version}
}
#Check to see if running Version 2.0 or better
[version]$xxVer = Get-PSVersion
If ($xxVer.Major -lt 2) {
	return @"
<prtg>
  <error>1</error>
  <text>The PRTG Backup Script requires Powershell v2.0 or greater</text>
</prtg>
"@
}

if (($BackupDataPath) -and ($BackupEXEPath)) {
	# Check target path
	$TargetPathExists = test-path -path "$BackupDataPath" -pathType container
	if (-not ($TargetPathExists)) {
		return @"
<prtg>
	<error>1</error>
	<text>BackupDataPath ($BackupDataPath) does not exist or is inaccessible</text>
</prtg>
"@
	}
	# Check target path
	$TargetPathExists = test-path -path "$BackupEXEPath" -pathType container
	if (-not ($TargetPathExists)) {
		return @"
<prtg>
	<error>1</error>
	<text>BackupEXEPath ($BackupEXEPath) does not exist or is inaccessible</text>
</prtg>
"@
	}
} elseif (($BackupTargetPath) ) {
	# Check target path
	$TargetPathExists = test-path -path "$BackupTargetPath" -pathType container
	if (-not ($TargetPathExists)) {
		return @"
<prtg>
	<error>1</error>
	<text>BackupTargetPath ($BackupTargetPath) does not exist or is in accessible</text>
</prtg>
"@
	}
	# Set Both
	$BackupDataPath = $BackupTargetPath
	$BackupEXEPath = $BackupTargetPath
} else {
	return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide either BackupTargetPath or BackupDataPath and BackupExePath</text>
</prtg>
"@
}

#
# Check for required parameters
if (-not $PRTGSourceServer ) {
	return @"
<prtg>
  <error>1</error>
  <text>Required parameter not specified: please provide PRTGSourceServer</text>
</prtg>
"@
}

if ($debuglevel -gt 0)
{ Write-Error -Message 'Aquiring Current' }
# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent()
if ($debuglevel -gt 0)
{ Write-Error -Message 'Aquiring Principal '+$myWindowsID.Name }
$myWindowsPrincipal = new-object System.Security.Principal.WindowsPrincipal($myWindowsID)

if ($debuglevel -gt 0)
{ Write-Error -Message 'Checking Role '+$myWindowsID.Name }

# Get the security principal for the Administrator role
#$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator
#if (-not $myWindowsPrincipal.IsInRole($adminRole)) {
#    $tmpStr = $myWindowsID.Name
#    return @"
#<prtg>
#  <error>1</error>
#  <text>Current User ($tmpStr) does not have elevated privileges</text>
#</prtg>
#"@
#}
########################################################################################

if ($debuglevel -gt 0)
{ Write-Error -Message 'Loading Modules ' }
# Add-Type -AssemblyName 'Microsoft.Win32.RegistryKey'
#Import-Module 'Microsoft.Win32.RegistryKey'
if ($debuglevel -gt 0) {
    [System.Reflection.Assembly]::LoadWithPartialName("System.web")
    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.Win32.RegistryKey")
#    [System.Reflection.Assembly]::LoadWithPartialName("System.Net")
  } else {
    [System.Reflection.Assembly]::LoadWithPartialName("System.web")| Out-Null
    [System.Reflection.Assembly]::LoadWithPartialName("Microsoft.Win32.RegistryKey")| Out-Null
#    [System.Reflection.Assembly]::LoadWithPartialName("System.Net")| Out-Null
  }
#

if ($debuglevel -gt 0)
{ Write-Error -Message 'Setting Constants ' }
$Script:constPRTGRegKeyRoot = "HKLM:\SOFTWARE\WOW6432Node\Paessler\PRTG Network Monitor\"
$Script:constPRTGRegKeySubPRTGServer = "Server"
$Script:constPRTGRegKeySubPRTGCore   = "Core"
$Script:constPRTGRegKeySubPRTGWebS   = "Webserver"
# Values
$Script:constPRTGRegKeyPRTGCore_DataPath = "Datapath"
$Script:constPRTGRegKeyPRTGCore_PgmPath = "Exepath"

# Structure Constants
$Script:constPRTGSrcDirType_Data = "PRTG-Data"
$Script:constPRTGSrcDirType_Pgm  = "PRTG-Pgm"

$Script:constPRTGDirType_Files = "Files"
$Script:constPRTGDirType_SensData = "SensorData"

#------------------------------------------------------------------------------------
# https://connect.microsoft.com/PowerShell/feedback/details/742760/add-type-cannot-add-type-the-type-name-already-exists
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining types ' }
add-type -ErrorAction SilentlyContinue @"
public struct BackupEntry {
   public string ChannelName;
   public string SrcDir;
   public string Directory;
   public string DirType;
   public string FilePattern;
   public string ExcludePattern;
   public int    RecurseDirs;
}
"@
if ($debuglevel -gt 0)
{ Write-Error -Message 'Setting Items to backup ' }
# SubDirectories
$Script:constPRTGSubdir_DirList = New-Object System.Collections.ArrayList
if($BackupInstall)
{
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "PRTG Install files";   SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "PRTG Installer Archive"; DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "PRTG Downloaded files"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "Download";                DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null;RecurseDirs = 1
	})| Out-Null
}# if($BackupInstall)
If ($BackupCustomSensors)
{
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Custom Sensors"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "Custom Sensors";   DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Custom Lookups"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "lookups\custom";   DirType = $Script:constPRTGDirType_Files;
		FilePattern = "*.ovl"; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "snmplibs"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "snmplibs";   DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "devicetemplates"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "devicetemplates";   DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
}# If ($BackupCustomSensors)
if($BackupCustom)
{
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Custom Header-Footer"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "webroot\includes";       DirType = $Script:constPRTGDirType_Files;
		FilePattern = "*_custom*.htm";        ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Custom Flow Rules"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "";       			   DirType = $Script:constPRTGDirType_Files;
		FilePattern = "*.osr";        	   ExcludePattern = $null; RecurseDirs = 0
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Vendor Icons"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "webroot\icons\devices";       DirType = $Script:constPRTGDirType_Files;
		FilePattern = "vendor*.*";        ExcludePattern = $null; RecurseDirs = 0
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Custom MapObjects"; SrcDir = $Script:constPRTGSrcDirType_Pgm;
		Directory = "webroot\mapobjects";       DirType = $Script:constPRTGDirType_Files;
		FilePattern = "*custom*.*";        ExcludePattern = $null; RecurseDirs = 0
	})| Out-Null
}# if($BackupCustom)


$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
	ChannelName = "Config Auto-Backups";      SrcDir = $Script:constPRTGSrcDirType_Data;
	Directory = "Configuration Auto-Backups"; DirType = $Script:constPRTGDirType_Files;
	FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
})| Out-Null

#-----------------------
# Files from PRTG Program Files directory
if($BackupData)
{
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Monitoring DB";     SrcDir = $Script:constPRTGSrcDirType_Data;
		Directory = "Monitoring Database"; DirType = $Script:constPRTGDirType_SensData;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Trap DB";     SrcDir = $Script:constPRTGSrcDirType_Data;
		Directory = "Trap Database"; DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
    $Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
            ChannelName = "Syslog DB"; SrcDir = $Script:constPRTGSrcDirType_Data;
            Directory = "Syslog Database"; DirType = $Script:constPRTGDirType_Files;
            FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
        })| Out-Null
    $Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
            ChannelName = "Toplist DB"; SrcDir = $Script:constPRTGSrcDirType_Data;
            Directory = "Toplist Database"; DirType = $Script:constPRTGDirType_Files;
            FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
        })| Out-Null
    $Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
            ChannelName = "SysInfo DB"; SrcDir = $Script:constPRTGSrcDirType_Data;
            Directory = "System Information Database"; DirType = $Script:constPRTGDirType_Files;
            FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
        })| Out-Null
    $Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
		ChannelName = "Ticket DB";     SrcDir = $Script:constPRTGSrcDirType_Data;
		Directory = "Ticket Database"; DirType = $Script:constPRTGDirType_Files;
		FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
	})| Out-Null
    $Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
            ChannelName = "Report PDFs"; SrcDir = $Script:constPRTGSrcDirType_Data;
            Directory = "Report PDFs"; DirType = $Script:constPRTGDirType_Files;
            FilePattern = $null; ExcludePattern = $null; RecurseDirs = 1
        })| Out-Null
}# if($BackupData)

#-------------------------------------------
# Add Custom directories To the list here
#-----------------------
#	$Script:constPRTGSubdir_DirList.Add([BackupEntry] @{
#		ChannelName = "MyChannelLabel"; SrcDir = "My File Source Directory";
#		Directory = "Sub Directory"; DirType = "{Files|Data} ";
#		FilePattern = {$null|"FilePattern}; ExcludePattern = {$null|"FilePattern}; RecurseDirs = {0|1}
#	})| Out-Null

#------------------------------------------------------------------------------------
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Return Type ' }
#------------------------------------------------------------------------------------
add-type -ErrorAction SilentlyContinue @"
public struct PRTGReturn {
   public int ErrorCode;
   public string Message;
   public string Results;
}
"@

$Script:PRTGRet = [PRTGReturn] @{ErrorCode=0; Message=""; Results=""}
#$Script:retVal.Message  = ""
#$Script:retVal.errorCode  = 0
#$Script:retVal.Results =""

#$reg = Get-WmiObject -List -Namespace root\default -ComputerName RemotePC -Credential "Domain\User" | Where-Object {$_.Name -eq "StdRegProv"}
#$reg.GetStringValue($HKLM,"SOFTWARE\Microsoft\Windows NT\CurrentVersion","ProductName").sValue


##
#$user = "Domain\Username"
#$pass = ConvertTo-SecureString "Password" -AsPlainText -Force
#$cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user,$pass
#$reg = Get-WmiObject -List -Namespace root\default -ComputerName $server -Credential $cred | Where-Object {$_.Name -eq "StdRegProv"}
#$HKLM = 2147483650
#$value = $reg.GetStringValue($HKLM,"Software\Microsoft\.NetFramework","InstallRoot").sValue
########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Get-RegKeyParts ' }
Function Get-RegKeyParts {
param(
   [string]$Path     = $(Read-Host "Remote Registry Path (must start with HKLM,HKCU,etc)")
   ,[ref]$rootHive
   ,[ref]$rootkey
   ,[ref]$messageString
   ,[switch]$Verbose
)
 $retVal = $false
if ($Verbose) { $VerbosePreference = 2 } # Only affects this script.

   $root, $last = $Path.Split("\")
   $last = $last[-1]
   $Path = $Path.Substring($root.Length + 1,$Path.Length - ( $last.Length + $Path.Length + 2))
   $root = $root.TrimEnd(":")

   #split the path to get a list of subkeys that we will need to access
   # ClassesRoot, CurrentUser, LocalMachine, Users, PerformanceData, CurrentConfig, DynData
   switch($root) {
      "HKCR"  { $rootHive = [Microsoft.Win32.RegistryHive]::ClassesRoot; retVal = true}
      "HKCU"  { $rootHive = [Microsoft.Win32.RegistryHive]::CurrentUser; retVal = true}
      "HKLM"  { $rootHive = [Microsoft.Win32.RegistryHive]::LocalMachine ; retVal = true}
      "HKU"   { $rootHive = [Microsoft.Win32.RegistryHive]::Users ; retVal = true}
      "HKPD"  { $rootHive = [Microsoft.Win32.RegistryHive]::PerformanceData; retVal = true}
      "HKCC"  { $rootHive = [Microsoft.Win32.RegistryHive]::CurrentConfig; retVal = true}
      "HKDD"  { $rootHive = [Microsoft.Win32.RegistryHive]::DynData; retVal = true}
      default {
		$messageString = "GetRegistry: Path argument is not valid"
		retVal = $false
		 }
   }
	return retVal
}

########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Get-LocalRegistry ' }
Function Get-LocalRegistry {
param(
    [Parameter(Position=0)] [string]$Path     = $(Read-Host "Remote Registry Path (must start with HKLM,HKCU,etc)")
   ,[Parameter(Position=1)] [ref]$messageString
)
 get-itemproperty -path $Path

}
########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Get-PRTGPaths ' }
Function Get-PRTGPaths {
#    [string]$cmpName #= $(Read-Host "Remote Computer Name")
param(
    [Parameter(Position=0)] [string]$compName
)
	# Remote: https://social.technet.microsoft.com/Forums/windowsserver/en-US/c4635703-2505-4aad-b32a-9cacb0c470ab/getitemproperty-on-remote-server?forum=winserverpowershell
	$retVal = "" | Select-Object Success,DPath,EPath,Message
	$retVal.Success = $false
	$PrtgCoreKey = $Script:constPRTGRegKeyRoot +$Script:constPRTGRegKeySubPRTGServer + "\"+ $Script:constPRTGRegKeySubPRTGCore
	$tmpRegKey = $null
	try
	{
		# $ArraySimple = @("localhost",$env:computername)
		# if ( $ArraySimple -Contains compName)
		$boolVal = ([string]::Compare($compName, "localhost", $True) -eq 0)  -Or ([string]::Compare($compName, $env:computername, $True) -eq 0)
		if (  $boolVal  )
		 {
			$tmpRegKey = Get-LocalRegistry $PrtgCoreKey
            if ($nul -ne $tmpRegKey)
			 {
				$retVal.DPath = $tmpRegKey.$Script:constPRTGRegKeyPRTGCore_DataPath
				$retVal.EPath = $tmpRegKey.$Script:constPRTGRegKeyPRTGCore_PgmPath
				$retVal.Success = $True
			 }
		 }
		else
		{
			$retVal.Message = "Currently only local PRTG Server supported."
			$retVal.Success = $false
		}
	}
	catch
	{
			$retVal.Message = $_

	}
	$retVal
}

########################################################################################
# https://stackoverflow.com/questions/29393153/parsing-robocopy-log-file-to-pscustomobject
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Get-PRTGPaths ' }
function Get-RoboCopyStats-FromOutput {
 [CmdletBinding()]
  param(
  [Parameter(ValueFromPipeline)]
	$ConsoleOutput
  )

  process {
	$statsOut = @()
	$stats = $ConsoleOutput | select -Last 6 | select -first 4

	$stats | % {
	  $s = $_ -split "\s+"
	  $o = new-object -type pscustomobject -property @{"Name"=$s[0];"Total"=$s[2];"Copied"=$s[3];"Skipped"=$s[4];"mismatch"=$s[5];"ErrorCnt"=0};
	  $statsOut += ,$o
	  } #Stats
	} # Process
 } # function Get-RoboCopyStats-FromOutput
########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Get-PRTGPaths ' }
function Get-RoboCopyStats-FromStringArray {
  param([string[]]$ConArr)
  process {
	$ErrCnt = 0
	$ErrMsg = ""
	$retVal = @()
	$ErrorLines = $ConArr -like "ERROR :*"
	if($ErrorLines.count -gt 0)
	{
	  $retVal += new-object -type pscustomobject -property @{"ErrorMessage"=$ErrorLines[0];"ErrorCnt"=1};
	}
	else
	{
		$stats = $ConArr | select -Last 6 | select -first 4
		$stats | % {
		  $s = $_ -split "\s+"
		  $o = new-object -type pscustomobject -property @{"Name"=$s[0];"Total"=$s[2];"Copied"=$s[3];"Skipped"=$s[4];"mismatch"=$s[5];"ErrorCnt"=0};
		  $retVal += ,$o
		  } #Stats
	}
   } # Process
 } # function Get-RoboCopyStats-FromOutput


########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function ExecCommand ' }
function ExecCommand($command) {
    if ($command[0] -eq '"') { Invoke-Expression "& $command" }
    else { Invoke-Expression $command }
}
########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function DoFileCopy ' }
Function DoFileCopy {
param(
	 [string]$SourcePath
	,[string]$TargetPath
	,[string]$CopyFiles
	,[string]$ExcludeFiles
	,[string]$ExcludeDir
	,[Bool]$RecurseSub
	,[string]$LogFile
)
	$retVal = @()
    # Build command
	 $CopyCMD = "robocopy """ + $SourcePath + """ """ + $TargetPath +""" "
	 if($CopyFiles -eq "")
	 {  $CopyFiles = "*"}
	 $CopyCMD = $CopyCMD +" " + $CopyFiles
	 if($ExcludeFiles -ne "")
	 {  $CopyCMD = $CopyCMD +" /XF " + $ExcludeFiles}
	 if($ExcludeDir -ne "")
	 {  $CopyCMD = $CopyCMD +" /XD " + $ExcludeDir}

	 if($RecurseSub)
	 {  $CopyCMD = $CopyCMD +" /E "}
	 if($LogFile -ne "")
	 {  $CopyCMD = $CopyCMD +" /LOG+:""" + $LogFile + """  /NP /TEE"}


	 $CopyCMD = $CopyCMD + " /ZB /MT /XJ /R:3 /BYTES"
		# robocopy $SourcePath $TargetPath $files /ZB /MT /XJ /XD Temp /R:3 /LOG:$log /NP
        # /XD Temp  # Temp directories shouldn't have important data files
		# /XF file [file]... :: eXclude Files matching given names/paths/wildcards.
        # /ZB   # copies files such that if they are interrupted part-way, they can be restarted (should be default IMHO)
        # /MT   # copies using multiple cores, good for many small files
        # /XJ   # Ignoring junctions, they can cause infinite loops when recursing through folders
        # /R:3  # Retrying a file only 3 times, we don't want bad permissions or other dumb stuff to halt the entire backup
		# /W:n :: Wait time between retries: default is 30 seconds.
        # /LOG:$log # Logging to a file internally, the best way to log with the /MT flag
		# /TEE :: output to console window, as well as the log file.
        # /NP   # Removing percentages from the log, they don't format well
	 $RoboCopyOutp = Invoke-Expression $CopyCMD # -OutVariable out | Get-RoboCopyStats-FromOutput
#	 $RoboCopyRes = Get-RoboCopyStats-FromStringArray -ConsoleOutput $RoboCopyOutp
	 #--------------------------------------------
	 	$ErrorLines = $RoboCopyOutp -like "ERROR :*"
		if($ErrorLines.count -gt 0)
		{
#	ERROR : You do not have the Backup and Restore Files user rights.
#	*****  You need these to perform Backup copies (/B or /ZB).
		  $retVal += new-object -type pscustomobject -property @{"ErrorMessage"=$ErrorLines[0];"ErrorCnt"=1};

		}
		else
		{ # https://stackoverflow.com/questions/29393153/parsing-robocopy-log-file-to-pscustomobject
#		[end-6]	               Total    Copied   Skipped  Mismatch    FAILED    Extras
#		[end-5]	    Dirs :         1         1         1         0         0         0
#		[end-4]	   Files :         2         0         2         0         0         0
#		[end-3]	   Bytes :  316.96 m         0  316.96 m         0         0         0
#		[end-2]	   Times :   0:00:00   0:00:00                       0:00:00   0:00:00
#		[end-1]	   Ended : Saturday, December 30, 2017 1:41:47 PM
#		[end  ]
		   $Footer = $RoboCopyOutp | select -Last 7
			$Footer | ForEach-Object {
				if ($_ -like "*Dirs*"){
					$lineAsArray = (($_.Split(':')[1]).trim()) -split '\s+'
					$Dirs = [pscustomobject][ordered]@{
						Total =  $lineAsArray[0]
						Copied =  $lineAsArray[1]
						Skipped =  $lineAsArray[2]
						Mismatch = $lineAsArray[3]
						FAILED = $lineAsArray[4]
						Extras = $lineAsArray[5]
					}
				}
				elseif ($_ -like "*Files*"){
					$lineAsArray = ($_.Split(':')[1]).trim() -split '\s+'
					$Files = [pscustomobject][ordered]@{
						Total =  $lineAsArray[0]
						Copied =  $lineAsArray[1]
						Skipped =  $lineAsArray[2]
						Mismatch = $lineAsArray[3]
						FAILED = $lineAsArray[4]
						Extras = $lineAsArray[5]
					}
				}
				elseif ($_ -like "*Bytes*"){
					$lineAsArray = ($_.Split(':',2)[1]).trim() -split '\s+'
					$Bytes = [pscustomobject][ordered]@{
						Total =  $lineAsArray[0]
						Copied =  $lineAsArray[1]
						Skipped =  $lineAsArray[2]
						Mismatch = $lineAsArray[3]
						FAILED = $lineAsArray[4]
						Extras = $lineAsArray[5]
					}
				}
<#				elseif ($_ -like "*Times*"){
					$lineAsArray = ($_.Split(':',2)[1]).trim() -split '\s+'
					$Times = [pscustomobject][ordered]@{
						Total =  $lineAsArray[0]
						Copied =  $lineAsArray[1]
						FAILED = $lineAsArray[2]
						Extras = $lineAsArray[3]
					}
#>
				} # $Footer | ForEach-Object {
				$retVal = [PSCustomObject]@{
						'Dirs'        = $Dirs
						'Files'       = $Files
						'Bytes'       = $Bytes
						'Times'       = $Times
						'ErrorCnt'    = 0
				}
			} # if($ErrorLines.count -gt 0) else
<#			$stats = $RoboCopyOutp | select -Last 6 | select -first 4
			$stats | % {
			  [string[]]$v = $_.Split(":")
			  [string[]]$s = $v[1].Split(" ")
			  $o = new-object -type pscustomobject -property @{"Name"=$v[0];"Total"=$s[2];"Copied"=$s[3];"Skipped"=$s[4];"mismatch"=$s[5];"ErrorCnt"=0};
			  $retVal += ,$o
			  } #Stats
#>
	 return  $retVal
}

########################################################################################
if ($debuglevel -gt 0)
{ Write-Error -Message 'Defining Function Backup-Files ' }
Function Backup-Files {
param(
	 [Object]$ObjectList
	,[string]$PRTGExePath
	,[string]$PRTGDataPath
	,[string]$TargetDataPath
	,[string]$TargetExePath
)

	 if(!($PRTGExePath.EndsWith("\")))
	 {  $PRTGExePath =  $PRTGExePath+  "\" }
	 if(!($PRTGDataPath.EndsWith("\")))
	 {  $PRTGDataPath =  $PRTGDataPath+  "\" }
	 if(!($TargetDataPath.EndsWith("\")))
	 {  $TargetDataPath =  $TargetDataPath+  "\" }
	 if(!($TargetExePath.EndsWith("\")))
	 {  $TargetExePath =  $TargetExePath+  "\" }

	if ($TargetExePath -eq $TargetDataPath )	{
		$TargetExePath	= $TargetExePath + "EXE\"
		$TargetDataPath	= $TargetDataPath + "Data\"
	}

	if (!(Test-Path -Path $TargetExePath ))
	{   New-Item -Force -ItemType directory -Path $TargetExePath }
	if (!(Test-Path -Path $TargetDataPath ))
	{   New-Item -Force -ItemType directory -Path $TargetDataPath }

	[string]$LogFile = $TargetDataPath + "PRTGBackupLog"
	if (!(Test-Path -Path $LogFile ))
	{   New-Item -Force -ItemType directory -Path $LogFile }
	[string]$TodayString = Get-Date -Format "yyyyMMdd-HHMM"
	$LogFile = $LogFile + "\" + $TodayString + "_Files.Log"

	#	[BackupEntry] @{
	#		ChannelName = "MyChannelLabel"; SrcDir = "My File Source Directory";
	#		Directory = "Sub Directory"; DirType = "{Files|Data} ";
	#		FilePattern = {$null|"FilePattern}; ExcludePattern = {$null|"FilePattern}; RecurseDirs = {0|1}
	#	}
	## Now go through each PSCustomObject In List
	foreach($BkupEntry in $ObjectList)
	{
		$FileSrcDir = $BkupEntry.SrcDir
		if($FileSrcDir -eq $Script:constPRTGSrcDirType_Data)
		{	$FileSrcDir = $PRTGDataPath	}
		if($FileSrcDir -eq $Script:constPRTGSrcDirType_Pgm)
		{	$FileSrcDir = $PRTGExePath	}

		if(-not ($FileSrcDir.EndsWith("\")))
		{  $FileSrcDir =  $FileSrcDir+  "\" }

		$FileSrcDir  = $FileSrcDir + $BkupEntry.Directory
		if($BkupEntry.SrcDir -eq $Script:constPRTGSrcDirType_Data)
		{	$FileDestDir = $TargetDataPath}
		elseif($BkupEntry.SrcDir -eq $Script:constPRTGSrcDirType_Pgm)
		{	$FileDestDir = $TargetExePath}

		$FileDestDir = $FileDestDir + $BkupEntry.Directory

		if ($debuglevel -gt 0)
		{ Write-Error -Message 'Backup ' + $FileSrcDir + ' to ' + $FileDestDir }

		$FileInclPatt = if ($null -eq $BkupEntry.FilePattern   ) { "" } else { $BkupEntry.FilePattern }
		$FileExclPatt= if ($null -eq $BkupEntry.ExcludePattern) { "" } else { $BkupEntry.ExcludePattern }
		$ExcludeDir  = ""
		# Check for existance and create if missing
		if(!(Test-Path -Path $FileDestDir ))
		{   New-Item -ItemType directory -Path $FileDestDir }

		$isSensData = $false
#$Script:constPRTGDirType_Files = "Files"
		if($BkupEntry.DirType -eq $Script:constPRTGDirType_SensData)
		{
			# Check for Compress
			if($CompressDataDirs)
			{   # Test Compress Attribute, set if it is not
				$ItemProp = (Get-ItemProperty -Path $FileDestDir).attributes
				If(!($ItemProp -band [io.fileattributes]::Compressed))
				{  #Set-ItemProperty -Path $FileDestDir -Name attributes `
					#	-Value ($ItemProp -BXOR [io.fileattributes]::Compressed)
					$Junk = "compact.exe /C /S:"""+$FileDestDir+""" "
					ExecCommand -command $Junk
				}
			}
			$ExcludeDir  = $TodayString
			$isSensData = $true
		}
		[Bool]$recurseSubdirs = ($BkupEntry.RecurseDirs -gt 0)
		# Make sure the path does not end in backslash, it is stored with it in the registry. Thanks to Noah & Dieter
		$FileSrcDir = $FileSrcDir.TrimEnd('\')
		$Res = DoFileCopy -SourcePath $FileSrcDir -TargetPath $FileDestDir -CopyFiles $FileInclPatt `
			-ExcludeFiles $FileExclPatt -ExcludeDir $ExcludeDir -RecurseSub $recurseSubdirs -LogFile $LogFile
		 #----------
		 if($null -ne $Res.ErrorCnt)
		 {
			 if($Res.ErrorCnt -gt 0)
			 {
				 $Script:PRTGRet.errorCode += 1
				 $Script:PRTGRet.Message += $BkupEntry.SrcDir +" [ "+ $Res.ErrorMessage +"]"
			 }
			 else
			 {

#	x = [pscustomobject][ordered]@{Total,  Copied, Skipped, Mismatch, FAILED, Extras }
#	y [PSCustomObject]@{ Dirs, Files, Bytes}  # Skipped: Times
				[string]$ChName = $BkupEntry.ChannelName
				if($isSensData)
				{
					[string]$totdirs    = "" + $Res.Dirs.Total
					if ($totdirs -eq "") { $totdirs = "0" }
					[string]$totskipped = "" + $Res.Dirs.Skipped
					if ($totskipped -eq "") {  $totskipped ="0" }
					[string]$totfailed = "" + $Res.Dirs.FAILED
					if ($totfailed -eq "")  { $totfailed = "0" }
					$Script:PRTGRet.Results += @"

	<result>
		<channel>$ChName Directories</channel>
		<value>$totdirs</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<result>
		<channel>$ChName Skipped</channel>
		<value>$totskipped</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
	<result>
		<channel>$ChName Failed</channel>
		<value>$totfailed</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
		<LimitMaxWarning>1</LimitMaxWarning>
		<LimitMode>1</LimitMode>
	</result>
"@
				}
                [string]$totfiles = "" + $Res.Files.Total
                [string]$xcomment = "" + $BkupEntry.Directory
                if ($totfiles -eq "") {
                  $totfiles = 0
				}
                $Script:PRTGRet.Results += @"

	<!-- Summary: $xcomment -->
	<result>
		<channel>$ChName Files</channel>
		<value>$totfiles</value>
		<Unit>Custom</Unit>
		<CustomUnit>#</CustomUnit>
	</result>
"@
			 }
		 }

	}

}
########################################################################################

# List of Directories to copy
# ProgramPath
#   Data
#		"\Monitoring Database\{Yesterdays Dir -> YYYMMDD}"
#   Default
#		"PRTG Installer Archive\PRTG Installer (Currently Installed Version).exe"
#   Custom
# "devicetemplates\*", "MIB\*", "Custom Sensors\*", "snmplibs\*", "lookups\custom\*", "webroot\icons\devices\*"
# Compress-Item -Path 'C:\Projects\Carbon' -OutFile 'C:\Carbon.zip' -UseShell
# $SourcePath = "D:\ProgramData\Paessler\PRTG Network Monitor"
# $TargetPath = $BackupTargetPath
#CopyFiles
# Zip
# http://stackoverflow.com/questions/1153126/how-to-create-a-zip-archive-with-powershell
#-----------------------------------------------------------------
if ($debuglevel -gt 0)
{ Write-Error -Message 'Declare Path Vars ' }
[switch]$Verbose = $True

	try
	{
		if($Script:PRTGRet.errorCode -eq 0)
		{
			if(-Not ($PRTGSrcDataPath -or $PRTGSrcEXEPath)) {
				if ($debuglevel -gt 0)
				{ Write-Error -Message 'Aquire Paths  ' }
				$Res = Get-PRTGPaths -compName $PRTGSourceServer #-Verboze $True
				#$Res = Get-GetPRTGPaths -cmpName "localhost" -dPat-cmpName  PRTGDataPathSrc -ePath [ref]$PRTGDataPathTgt -messageString [ref]$messageString
				if($Res.Success) {
					$PRTGSrcDataPath = $Res.DPath
					$PRTGSrcEXEPath = $Res.EPath
				}
			}
			if($PRTGSrcDataPath -and $PRTGSrcEXEPath) {
				if ($debuglevel -gt 0)
				{ Write-Error -Message 'Backup Dirs ' }
				Backup-Files -ObjectList $Script:constPRTGSubdir_DirList -PRTGExePath $PRTGSrcEXEPath -PRTGDataPath $PRTGSrcDataPath `
					-TargetDataPath $BackupDataPath -TargetExePath $BackupExePath
			}
			else
			{
				$Script:PRTGRet.errorCode  = 1
				$Script:PRTGRet.Message = $Res
			}
		}
	}
	catch
	{
		$Script:PRTGRet.errorCode  = 1
		$Script:PRTGRet.Message = "" + $_
	}

$MsgTxt = $Script:PRTGRet.Message
$ResTxt = $Script:PRTGRet.Results

if ($Script:PRTGRet.errorCode -gt 0)
{
	return @"
<prtg>
$ResTxt
  <error>1</error>
  <text>$MsgTxt</text>
</prtg>
"@
}
else
{
	if ($MsgTxt -eq "")
	 {
		 $MsgTxt = "Backup script succeeded"
	 }
	return @"
<prtg>
$ResTxt
  <text>$MsgTxt</text>
</prtg>
"@
}